JobManagement::Engine.routes.draw do

  # Kaminari Pagination
  concern :paginatable do
    get '(page/:page)', action: :index, on: :collection, as: ''
  end

  concern :searchable do
    get 'search/reset', action: :reset_session_search_params, on: :collection
    # Erlaube das Absenden des Suchformulars mittels Post,
    # da es ab ca. 2000 Zeichen in der URL knallt..
    post 'search', action: 'index', as: :search, on: :collection
  end

  resources :jobs, except: [:update, :edit], concerns: [:paginatable, :searchable] do
    get 'download'
  end

end
