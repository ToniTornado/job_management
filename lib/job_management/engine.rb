module JobManagement
  class Engine < ::Rails::Engine
    isolate_namespace JobManagement

    require 'paperclip'
    require 'sidekiq'
    require 'active_model_attributes'

    # Load decorator classes from main app
    config.to_prepare do
      Dir.glob(Rails.root + "app/decorators/**/*_decorator*.rb").each do |c|
        require_dependency(c)
      end
    end

    config.before_initialize do
      config.i18n.load_path += Dir["#{config.root}/config/locales/*.yml"]
    end

  end
end
