require 'active_record'

namespace :job_maintenance do
  desc "Cleanup jobs"
  task :cleanup_jobs => :environment do
    JobManagement::Job.where(status: 'running').each do | job |
      job.status='finished'
      job.message=I18n.t('jobs.finished_by_restart')
      job.save
    end
  end
end
