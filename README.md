
# JobManagement Gem

This project contains the ruby gem **JobManagement**. This gem can be used
by Ruby-on-Rails applications which want to run background jobs in parallel to
the rails server. This gem provides the needed functionality and the views for
managing background jobs.

# Installation

## Prerequisist
Your application should be based on Ruby-on-Rails > 5.0.0.

## Install Gem

* Chage the source in your ``Gemfile`` to a repository containing this gem.
We suggest to replace the source line by this block of code:
```
if ENV['GEMREPO']
    source ENV['GEMREPO']
else
    source 'http://nexus.it.gefa.de/nexus/content/repositories/rubygems'
end
```
You could then overwrite your source repository with your own by setting the enviroment
variable ``GEMREPO`` if you haven't got access to the GEFA gem repository.

* Add this Gem to your Gemfile: `gem 'job_management'`
* run `bundle install`
* prepare your database:
```
rails generate delayed_job:active_record
rake db:migrate
```

## Prepare application
Add an initializer ``config/delayed_job_config.rb`` with the following content:
```
Delayed::Worker.destroy_failed_jobs = false
Delayed::Worker.sleep_delay = 60
Delayed::Worker.max_attempts = 3
Delayed::Worker.max_run_time = 2.days
Delayed::Worker.read_ahead = 10
Delayed::Worker.delay_jobs = !Rails.env.test?
Delayed::Worker.raise_signal_exceptions = :term
Delayed::Worker.logger = Logger.new(File.join(Rails.root, 'log', 'delayed_job.log'))
```

## Setup workers
If you add a new job it won't be processed until you start one or multiple workers:
```
bundle exec rake jobs:work
```

If you want the worker to be stoped automatically after a job is completed add this to your settings.yml:
```
exit_delayed_job_on_complete: true
```

This can be useful if you want to be sure that the memory allocated by the worker is completely freed. Be ware: you have to restart the worker if you want your jobs to run.
