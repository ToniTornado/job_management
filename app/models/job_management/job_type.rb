module JobManagement
  class JobType < ActiveRecord::Base

    def to_s
      class_name.gsub(/::/, ' > ')
    end

    def underscore_name
      class_name.underscore
    end

    def localized_name
      I18n.t("my_jobs.#{name}") rescue name
    end

    def localized_description
      I18n.t("my_jobs.#{name}_description") rescue name
    end

    def self.select_collection(include_inactive=false)
      options = []
      job_types = include_inactive ? JobType.all : JobType.where(active: true)
      job_types.each do |job_type|
        options << [job_type.localized_name, job_type.class_name]
      end
      options.sort { |a,b| a[0] <=> b[0] }
    end

  end
end
