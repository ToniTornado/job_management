module JobManagement
  class Job < ActiveRecord::Base

    belongs_to :job_type

    belongs_to :jobable, polymorphic: true

    ransacker :started_at_casted do |parent|
      Arel.sql('date(job_management_jobs.started_at)')
    end

    serialize :options

    has_attached_file :file
    validates_attachment :file, content_type: {
      content_type: [
        # Excel
        "application/vnd.ms-excel",
        "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
        # Word
        "application/msword",
        "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
        # XML
        "application/xml",
        # ZIP
        "application/zip",
        "application/x-compressed-zip",
        # PDF
        "application/pdf",
        # Text
        "text/plain",
        # CSV
        "text/csv", "text/comma-separated-values",
        # Leere Datei
        "inode/x-empty"
      ]
    }

    @@status_option_values = {
      new: I18n.t('job.status.new'),
      running: I18n.t('job.status.running'),
      finished: I18n.t('job.status.finished'),
      error: I18n.t('job.status.failed')
    }

    def self.status_options
      @@status_option_values.invert
    end

    def display_status_option
      @@status_option_values[status.to_sym]
    end

    def self.display_status_for(symbol)
      @@status_option_values[symbol]
    end

    @@status_option_class_names = {
      new: 'success',
      running: 'warning',
      finished: 'primary',
      error: 'danger'
    }

    def status_option_class_name
      @@status_option_class_names[status.to_sym]
    end

    def self.status_option_class_name_for(symbol)
      @@status_option_class_names[symbol]
    end

    def status_url
      "/api/jobs/status/#{id}"
    end

    def is_running?
      status == 'running'
    end

  end
end
