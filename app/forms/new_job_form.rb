class NewJobForm
  include ActiveModel::Model
  include ActiveModel::Callbacks
  include ActiveModel::Validations
  include ActiveModel::Validations::Callbacks
  # Hopefully supported natively one day
  include ActiveModelAttributes

  attr_accessor :job_type_class_name, :audit_user_id

  validates :job_type_class_name, presence: true

  def initialize(params = {})
    Dir["app/forms/jobs/*"].each do |file|
      self.class.send :include, "Jobs::#{File.basename(file,'.rb').camelize}".constantize
    end
    super
  end

  def jobable
    if jobable_type && jobable_id
      return jobable_type.constantize.find(jobable_id) rescue nil
    end
  end

end
