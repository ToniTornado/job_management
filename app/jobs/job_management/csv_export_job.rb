module JobManagement
  class CsvExportJob < JobManagement::ManagedJob

    # To use this job you have to create the JobType before like this:
    # JobManagement::JobType.find_or_create_by!(class_name: "JobManagement::CsvExportJob", name: "csv_export_job", description: "import_from_ksiop_job_description")

    def perform(options={})
      exported_class = options[:exported_class].constantize
      query = exported_class.ransack(options[:search_params])
      query.sorts = 'id asc'
      collection = query.result

      exported_fields = options[:exported_fields].map { |x| x.to_sym }
      translated_fields = options[:translated_fields].map { |x| x.to_sym }
      object_class_name = exported_class.model_name.human(count: 2)
      if options[:original_filename]
        original_filename = options[:original_filename]
      else
        original_filename = "#{object_class_name}-#{SecureRandom.uuid}.csv"
      end
      csv_data = CsvExport.csv_data_for(collection, exported_fields, translated_fields)

      file = StringIO.new(csv_data)

      content_type ='text/csv; ' + Settings.csv.export_encoding  + '; header=present',
      metaclass = class << file; self; end
      metaclass.class_eval do
        define_method(:original_filename) { original_filename }
        define_method(:content_type) { content_type }
      end
      {message: "#{collection.count} #{object_class_name} #{I18n.t('jobs.exported')}", file: file}
    end

    def after(job)
      puts "PID: #{Process.pid}"
    end

  end
end
