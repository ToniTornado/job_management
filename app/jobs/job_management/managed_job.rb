module JobManagement
  class ManagedJob < ActiveJob::Base
    queue_as :default

    def job_type
      job_type = JobType.find_by(class_name: self.class.name)
      raise "no job_type existing for #{self.class.name}" unless job_types.present?
      job_type
    end

    def job
      Job.find_by(active_job_id: self.job_id)
    end

    before_enqueue do |active_job|
      create_job_for_active_job(active_job)
    end

    around_perform do |active_job, block|
      # Wurde ein Nutzer angegeben, in dessen Namen mögliche Änderungen durchgeführt werden sollen?
      @audit_user_id = active_job.arguments.first[:audit_user_id] rescue nil
      job = Job.find_by(active_job_id: active_job.job_id) || create_job_for_active_job(active_job)
      job.started_at = Time.now
      job.message = I18n.t('jobs.running')
      job.status = 'running'
      job.save!

      begin
        # perform
        result = block.call
        job.message = result[:message]
        job.finished_at = Time.now
        job.status = 'finished'
        if result[:file].is_a? Pathname
          File.open(result[:file]) do |f|
            job.file = f
            job.save!
          end
          File.delete(result[:file])
        else
          job.file = result[:file]
          job.save!
        end
      rescue Exception => e
        job.finished_at = Time.now
        # Erste 3 Zeilen vom Backtrace speichern
        job.message = "#{e.message}\n\n#{e.backtrace[0..20].join("\n")}"
        job.status = 'error'
        job.save!
      end
      GC.start
    end

    def create_job_for_active_job(active_job)
      job_options = active_job.arguments.first || {}
      # Falls der Job zu einem anderen Objekt gehört
      if job_options[:jobable_type] && job_options[:jobable_id]
        jobable = job_options[:jobable_type].constantize.find(job_options[:jobable_id]) rescue nil
      end
      job_type = JobType.find_by(class_name: self.class.to_s)
      created_by = User.find(job_options[:audit_user_id]).userid rescue nil
      Job.create!(
        message: I18n.t('jobs.queued'),
        active_job_id: active_job.job_id,
        enqueued_at: Time.now,
        job_type: job_type,
        options: job_options,
        jobable: jobable,
        created_by: created_by
      )
    end

  end
end
