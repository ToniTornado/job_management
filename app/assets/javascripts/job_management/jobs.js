$(function() {
  $('#new_job_form_job_type_class_name').change(function(obj) {
    $('#dependent_forms select, #dependent_forms input, #dependent_forms selectize-input').prop('disabled', true)
    $('#dependent_forms').children().each(function(idx, itm) {
      $(itm).addClass('hidden')
    })
    $('#' + $(obj.target).val()).removeClass('hidden')
    $('#' + $(obj.target).val() + ' select, ' + '#' + $(obj.target).val() + ' input, ' + '#' + $(obj.target).val() + ' selectize-input').prop('disabled', false)
    $('#' + $(obj.target).val() + ' select, ' + '#' + $(obj.target).val() + ' input').removeClass('disabled')
    $('#' + $(obj.target).val() + ' .selectized').each(function(idx, el) {
      $(el).selectize()[0].selectize.enable()
    })
  })
  $('#new_job_form_job_type_class_name').change()
})
