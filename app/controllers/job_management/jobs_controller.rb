module JobManagement
  class JobsController < JobManagement::ApplicationController

    before_action :set_job, only: [:show, :update, :destroy, :download]

    def index
      @jobs = Job.all
      if @jobable.present?
        @jobs = @jobs.where(jobable: @jobable)
      end
      @q = @jobs.ransack(@search_params)
      @q.sorts = 'id desc' if @q.sorts.empty?
      @jobs = @q.result
      @jobs = @jobs.page(params[:page]).per(@per_page)
    end

    def show
    end

    def new
      @new_job_form = NewJobForm.new
      @new_job_form.audit_user_id = current_user.id
    end

    def download
      file = @job.file
      if file.options[:storage] == :s3
        data = open(file.expiring_url(10))
        send_data data.read, type: data.content_type, x_sendfile: true, filename: file.original_filename
      elsif file.options[:storage] == :filesystem
        send_file file.path, filename: file.original_filename
      end
    end

    def create
      @new_job_form = NewJobForm.new(new_job_form_params)
      if @new_job_form.valid?
        job_class = new_job_form_params[:job_type_class_name].constantize
        job_class.perform_later(new_job_form_params)
        redirect_to(:jobs, notice: t(:object_create_success_html, name: Job.model_name.human, label: job_class.name).html_safe)
      else
        render 'new'
      end
    end

    def destroy
      label = @job.job_type.localized_name rescue '-'
      unless @job.status == 'running'
        @job.destroy
        redirect_to(jobs_path, notice: t(:object_delete_success_html, name: Job.model_name.human, label: label).html_safe)
      else
        redirect_to(jobs_path, notice: t(:object_delete_failure_html, name: Job.model_name.human, label: label).html_safe)
      end
    end

    private

      def set_job
        @job = Job.find(params[:id] || params[:job_id])
      end

      def new_job_form_params
        # Erlaube hier alle dynamischen Parameter,
        # der Job benutzt ohnehin nur die erlaubten
        params.require(:new_job_form).permit!.to_h
      end

    end
  end
