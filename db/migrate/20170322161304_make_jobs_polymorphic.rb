class MakeJobsPolymorphic < ActiveRecord::Migration[5.0]
  def change
    add_column :job_management_jobs, :jobable_id, :integer
    add_column :job_management_jobs, :jobable_type, :string
    add_index :job_management_jobs, [:jobable_type, :jobable_id]
  end
end
