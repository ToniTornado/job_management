class CreateJobTypes < ActiveRecord::Migration
  def up
    create_table :job_management_job_types do |t|
      t.string :name
      t.string :description
      t.string :class_name

      t.timestamps
    end
  end

  def down
    drop_table :job_management_job_types
  end
end
