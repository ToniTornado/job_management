class CreateJobs < ActiveRecord::Migration
  def up
    create_table :job_management_jobs do |t|
      t.string :status, default: 'new'
      t.string :message
      t.text :options
      t.string :active_job_id
      t.datetime :enqueued_at
      t.datetime :started_at
      t.datetime :finished_at
      t.references :job_type

      t.attachment :file

      t.timestamps null: false
    end
    add_index :job_management_jobs, :active_job_id
  end

  def down
    drop_table :job_management_jobs
  end
end
