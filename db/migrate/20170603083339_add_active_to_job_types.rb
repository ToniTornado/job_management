class AddActiveToJobTypes < ActiveRecord::Migration[5.0]
  def change
    add_column :job_management_job_types, :active, :boolean, default: true
  end
end
