$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "job_management/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "job_management"
  s.version     = JobManagement::VERSION
  s.authors     = ["Robin Wielpuetz", "Christian Noack"]
  s.email       = ["robin.wielpuetz@gmail.com", "christian.noack@agile-methoden.de"]
  s.homepage    = "http://www.agile-methoden.de"
  s.summary     = "Engine for background job management."
  s.description = "Engine for background job management."

  s.files = Dir["{app,config,db,lib}/**/*", "Rakefile", "README.md"]

  s.add_dependency "rails", "~> 5.0.1"
  s.add_dependency 'sidekiq'
  s.add_dependency 'paperclip'
  s.add_dependency 'active_model_attributes'

end
